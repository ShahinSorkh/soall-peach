package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

var count = 0

func Increment(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}

	inputBytes, readErr := ioutil.ReadAll(r.Body)

	if readErr != nil {
		http.Error(w, readErr.Error(), http.StatusInternalServerError)
		return
	}

	inputString := string(inputBytes)
	input, convErr := strconv.Atoi(strings.TrimSpace(inputString))

	if convErr != nil {
		http.Error(w, convErr.Error(), http.StatusInternalServerError)
		return
	}

	count += input
	fmt.Fprintf(w, "%d", count)
}

func GetCount(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}

	fmt.Fprintf(w, "%d", count)
}

func main() {
	http.HandleFunc("/", Increment)
	http.HandleFunc("/count", GetCount)
	http.ListenAndServe(":80", nil)
}
