/*
function is_prime(n)
    if n ≤ 3 then
        return n > 1
    else if n mod 2 = 0 or n mod 3 = 0
        return false

    let i ← 5

    while i × i ≤ n do
        if n mod i = 0 or n mod (i + 2) = 0
            return false
        i ← i + 6

    return true
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define OUTPUT_BUFFER_SIZE 4096
#define MAX_LINE_LENGTH 2048

char is_prime(int n)
{
    if (n <= 3) {
        return n > 1 ? 1 : 0;
    } else if (n % 2 == 0 || n % 3 == 0) {
        return 0;
    }

    double root = sqrt(n);
    for (int i = 5; i <= root; i+=6) {
        if (n % i == 0 || n % (i + 2) == 0) {
            return 0;
        }
    }

    return 1;
}

int main(int argc, char** argv)
{
    if (argc < 1) {
        return EXIT_FAILURE;
    }

    char* filename = argv[1];
    FILE* file = fopen(filename, "r");

    if (!file) {
        perror("File could not be loaded\n");
        perror(filename);
        return EXIT_FAILURE;
    }

    char output[OUTPUT_BUFFER_SIZE];
    memset(output, '\0', sizeof(output));

    setvbuf(stdout, output, _IOFBF, OUTPUT_BUFFER_SIZE);

    int n;
    while (!feof(file)) {
        fscanf(file, "%d", &n);
        printf("%d", is_prime(n));
        if (!feof(file)) printf("\n");
    }

    fclose(file);
    fflush(stdout);

    return EXIT_SUCCESS;
}
